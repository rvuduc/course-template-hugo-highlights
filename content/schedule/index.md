+++
categories = []
date = "2016-08-16T22:56:20-04:00"
tags = []
title = "Schedule"

+++

The schedule posted below is subject to change. The <i class="fa fa-hourglass-end"></i>
icon indicates an assignment is due that week.

> Unless otherwise specified, assignments are due on Mondays at 11:59 PM AOE. For
> example, the "Course Survey" is due on **Monday, August 22 at 11:59 PM AOE**,
> which is the same as **Tuesday, August 23 at 7:59 AM** Eastern US time.


Date          | Topics
--------------|-------------------------------------------------------
Aug 22-24     | CS 101 by a motivating example: Association rule mining (big-Oh, ADTs, sparse matrices, [Python](http://www.cc.gatech.edu/~simpkins/teaching/python-bootcamp/august2016.html))
              |     <i class="fa fa-hourglass-end"></i> Course survey
Aug 29-31     | Data collection: Requests (web API) + zipfile, BeautifulSoup
              |     <i class="fa fa-hourglass-end"></i> Lab 1
Sep 5-7       | **(No class Sep 5: Labor Day)** Preprocessing: Unstructured files, regular expressions (Enron email archive)
              |     <i class="fa fa-hourglass-end"></i> **Wed Sep 7**: Lab 2
Sep 12-14     | Data organization 1: Relational databases (SQL) (Yelp DB)
              |     <i class="fa fa-hourglass-end"></i> Lab 3
Sep 19-21     | Data visualization 1 $\subseteq \\{\mathtt{seaborn}, \mathtt{plot.ly}, \mathtt{bokeh}\\}$
              |     <i class="fa fa-hourglass-end"></i> Lab 4
Sep 26-28     | PageRank (Political blogs)
              |     <i class="fa fa-hourglass-end"></i> Lab 5
Oct 3-5       | Linear algebra review and numerical computing in Python (Numpy)
              |     <i class="fa fa-hourglass-end"></i> Lab 6
Oct 10-12     | **(No class Oct 10: Fall Break)** Linear regression
              |     <i class="fa fa-hourglass-end"></i> **Wed Oct 12**: Lab 7
Oct 17-19     | K-means
              |     <i class="fa fa-hourglass-end"></i> Lab 8
Oct 24-26     | Functional programming (Intro to [R](http://r-project.org))
              |     <i class="fa fa-hourglass-end"></i> Lab 9
Oct 31-Nov 2  | Data organization 2: The [Tidy](http://vita.had.co.nz/papers/tidy-data.pdf) paradigm and Data visualization 2: GoG and `ggplot`
              |     <i class="fa fa-hourglass-end"></i> Lab 10
Nov 7-9       | Logistic regression
              |     <i class="fa fa-hourglass-end"></i> Lab 11
Nov 14-16     | **(No class; instructors on travel)**
              |
Nov 21-23     | **(No class Nov 23: Thanksgiving)** Numerical optimization
              |     <i class="fa fa-hourglass-end"></i> Lab 12
Nov 28-30     | SVD and friends
              |     <i class="fa fa-hourglass-end"></i> Lab 13
Dec 5-7       | (special topics: TBD)
              |     <i class="fa fa-hourglass-end"></i> Lab 14
              |
Wed Dec 14    | <i class="fa fa-stop"></i> **Final Exam:** Meet in Klaus 2447

